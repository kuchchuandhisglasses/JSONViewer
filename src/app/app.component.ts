import { Component } from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  requiredFields = [];
  primaryAttr = '';
  JSONArrayString = '';
  JSONArray: Array<Object>;
  KeyList: Map<Object, any> = new Map<Object, any>();
  setJSONArray(inputString: string) {
    const jsonString = '{ "details" : [' + inputString + '] }';
    if (JSON.parse(jsonString).details) {
      this.JSONArray = JSON.parse(jsonString).details;
      for (const element of this.JSONArray) {
          this.KeyList.set(element, Object.keys(element));
      }
      debugger;
    }
  }
  setRequiredFieldsArray(inputString: String) {
    this.requiredFields = inputString.split(',');
  }
}
